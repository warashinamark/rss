package jp.newgreat.rss.result;

import java.util.ArrayList;
import java.util.List;

import jp.newgreat.rss.models.Rss10;
import jp.newgreat.rss.models.Rss20;
import jp.newgreat.rss.models.Rss20.Channel;
import jp.newgreat.rss.models.Rss20.Item;
import jp.newgreat.rss.models.RssIF;

public abstract class BaseResult implements ResultIF {
	private RssIF rssIF= null;
	public BaseResult(RssIF rssIF){
		this.rssIF = rssIF;
	}
	abstract protected void doOutput(String lineArg);
	protected String[] getOutput(Channel channelArg){
		ArrayList<String> rtnList = new ArrayList<String>();
		rtnList.add("[Channel]title:"+channelArg.getTitle());
		rtnList.add("[Channel]link:"+channelArg.link);
		rtnList.add("[Channel]description:"+channelArg.getDescription());
        for (Item elm: channelArg.getItems()){
            rtnList.add("[item]title:"+elm.getTitle());
            rtnList.add("[item]link:"+elm.link);
            rtnList.add("[item]pubDate:"+elm.pubdate);
            rtnList.add("[item]description:"+elm.getDescription());}
        return rtnList.toArray(new String[0]);
	}
	protected String[] getOutput(Rss10.Channel channelArg){
		ArrayList<String> rtnList = new ArrayList<String>();
		rtnList.add("[Channel]title:"+channelArg.title);
		rtnList.add("[Channel]link:"+channelArg.link);
		rtnList.add("[Channel]date:"+channelArg.dc_date);
		rtnList.add("[Channel]description:"+channelArg.description);
		return rtnList.toArray(new String[0]);
	}
	protected String[] getOutput(List<Rss10.Item> itemListArg){
		ArrayList<String> rtnList = new ArrayList<String>();
		for ( Rss10.Item elm : itemListArg){
			rtnList.add("[item]title:"+elm.title);
			rtnList.add("[item]link:"+elm.link);
			rtnList.add("[item]date:"+elm.date);
			rtnList.add("[item]description:"+elm.description);}
		return rtnList.toArray(new String[0]);
	}
	public RssIF doOutput() {
		if ( rssIF instanceof Rss20){
			Rss20 rss20 = (Rss20)rssIF;
			Channel channel = (Channel)rss20.channel;
			for ( String line : getOutput(channel)){
				doOutput( line );}}
		else if( rssIF instanceof Rss10){
			Rss10 rss10 = (Rss10)rssIF;
			Rss10.Channel channel = (Rss10.Channel)rss10.channel;
			for ( String line : getOutput( channel )){
				doOutput( line );}
			List<Rss10.Item> items = rss10.items;
			for ( String line : getOutput( items )){
				doOutput( line );}}
		return rssIF;
	}
}//Unreachable