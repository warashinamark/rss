package jp.newgreat.rss;

import jp.newgreat.rss.reader.UrlReader;
import jp.newgreat.rss.result.TerminalAcceptee;
import jp.newgreat.rss.util.Constants.Mode;
import jp.newgreat.rss.util.LogUtils;

//エントリーポイント
public class RssCollector {
	public static void main(String[] args){
		RssCollector self = new RssCollector();
		for ( String arg : args ){
			self.doTraverse( arg );}
	}
	//compositionは不在だが、traverse
	//visitorのポリモーフィズムで線形にメソッド呼び出しが起こる。
	private void doTraverse(String arg){
		//エラー出力先を指定
		LogUtils.setMode(Mode.FILE);
		AccepteeIF accept = new UrlReader(arg);
		//ビジネスロジックループ
		LogicVisitor v = new LogicVisitor();
		while ( !(accept instanceof TerminalAcceptee)){
			accept = accept.accept(v);
		}
	}
}